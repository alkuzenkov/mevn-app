import Vue from 'vue'
import Router from 'vue-router'
import ClientsList from '@/components/clients/List'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'ClientsList',
      component: ClientsList
    }
  ]
})

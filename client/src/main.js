import 'bootstrap-css-only/css/bootstrap.min.css';
import 'mdbvue/build/css/mdb.css';

import 'es6-promise/auto';
import Vue from 'vue'
import store from './store'
import router from '@/router/index'

new Vue({
  el: '#app',
  router,
  store,
  template: '<router-view/>'
})

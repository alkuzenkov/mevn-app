import clientService from '@/services/client'
import {cloneDeep, findIndex} from 'lodash'
import Vue from 'vue'

export default {
    namespaced: true,
    state: {
        all: [],
        form: {
            active: false,
            data: {},
            error: {},
            loading: false
        },
    },
    getters: {},
    actions: {
        /**
         * Load all clients and save them to store
         * @param {Function} commit
         * @returns {Promise<void>}
         */
        async loadAll({commit}) {
            commit('setClients', await clientService.all())
        },
        /**
         * Actions after client form submitting
         * @param {Object} state
         * @param {Function} commit
         * @returns {Promise<void>}
         */
        async submitForm({state, commit}) {
            commit('setFormLoading', true)
            try {
                let {model} = await clientService.save(state.form.data)
                commit('updateClientsWith', model)
                commit('cleanForm')
            } catch ({error}) {
                commit('setFormError', error)
            }
            commit('setFormLoading', false)
        },
        /**
         * Delete client
         * @param {Function} commit
         * @param {Object} client
         * @returns {Promise<void>}
         */
        async deleteClient({commit}, client) {
            try {
                await clientService.remove(client)
                commit('deleteClient', client)
            } catch ({error}) {
                alert(error)
            }
        },
    },
    mutations: {
        /**
         * Set clients to store
         * @param {Object} state
         * @param {Array} clients
         */
        setClients(state, clients) {
            state.all = clients
        },
        /**
         * Set client to client form
         * @param {Object} state
         * @param {Object} client
         */
        activateClientForm(state, client) {
            state.form.data = client
                ? cloneDeep(client)
                : {
                    _id: null,
                    name: '',
                    email: '',
                    phone: '',
                    providers: []
                }
            state.form.active = true
        },
        /**
         * Set name in client form
         * @param {Object} state
         * @param {String} name
         */
        setFormName(state, name) {
            state.form.data.name = name
        },
        /**
         * Set email in client form
         * @param {Object} state
         * @param {String} email
         */
        setFormEmail(state, email) {
            state.form.data.email = email
        },
        /**
         * Set phone in client form
         * @param {Object} state
         * @param {String} phone
         */
        setFormPhone(state, phone) {
            state.form.data.phone = phone
        },
        /**
         * Update store with saved client
         * @param {Object} state
         * @param {Object} client
         */
        updateClientsWith(state, client) {
            let i = findIndex(state.all, {_id: client._id})
            if (i !== -1) {
                Vue.set(state.all, i, client)
            } else {
                state.all.push(client)
            }
        },
        /**
         * Enable/disable form preloader
         * @param {Object} state
         * @param {Boolean} loading
         */
        setFormLoading(state, loading) {
            state.form.loading = loading
        },
        /**
         * Set errors of client update
         * @param {Object} state
         * @param {Object} error
         */
        setFormError(state, error) {
            state.form.error = error
        },
        /**
         * Set default values of client form
         * @param {Object} state
         */
        cleanForm(state) {
            state.form = {
                active: false,
                data: {},
                error: {},
                loading: false
            }
        },
        /**
         * Delete client
         * @param {Object} state
         * @param {Object} client
         */
        deleteClient(state, client) {
            let i = findIndex(state.all, {_id: client._id})
            if (i !== -1) {
                state.all.splice(i, 1)
            }
        },
        /**
         * Check/uncheck provider in client form
         * @param {Object} state
         * @param {Object} provider
         */
        toggleProviderInForm(state, provider) {
            let i = findIndex(state.form.data.providers, {_id: provider._id})
            if (i !== -1) {
                state.form.data.providers.splice(i, 1)
            } else {
                state.form.data.providers.push({
                    _id: provider._id
                })
            }
        }
    }
}

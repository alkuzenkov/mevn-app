import providerService from '@/services/provider'
import {findIndex} from 'lodash'
import Vue from 'vue'

export default {
    namespaced: true,
    state: {
        all: [],
        forms: [],
    },
    getters: {},
    actions: {
        /**
         * Get all providers and set them to store
         * @param {Object} state
         * @param {Function} commit
         * @returns {Promise<void>}
         */
        async loadAll({state, commit}) {
            let providers = await providerService.all()
            commit('setProviders', providers)
            state.all.map(provider => commit('addToProviderForm', provider))
        },
        /**
         * Actions after form submit
         * @param {Object} state
         * @param {Function} commit
         * @param {Number} form_index
         * @returns {Promise<any>}
         */
        submitForm({state, commit}, form_index) {
            return new Promise(async (resolve, reject) => {
                commit('setFormProviderLoading', {
                    form_index,
                    loading: true
                })
                try {
                    let {model} = await providerService.save(state.forms[form_index])
                    commit('updateProvidersWith', model)
                    commit('saveFormAndClose', {
                        provider: model,
                        form_index
                    })
                    resolve()
                } catch ({error}) {
                    if (error) {
                        commit('setFormProviderError', {
                            form_index,
                            error
                        })
                    }
                    reject()
                } finally {
                    commit('setFormProviderLoading', {
                        form_index,
                        loading: false
                    })
                }
            })
        },
        /**
         * Delete provider
         * @param {Object} state
         * @param {Function} commit
         * @param {Number} form_index
         * @returns {Promise<void>}
         */
        async deleteForm({state, commit}, form_index) {
            try {
                let {model} = await providerService.remove(state.forms[form_index])
                commit('deleteProviderForm', form_index)
                commit('deleteProvider', model)
            } catch ({error}) {
                commit('setFormProviderError', {
                    form_index,
                    error
                })
            }
        }
    },
    mutations: {
        /**
         * Set providers to state
         * @param {Object} state
         * @param {Array} providers
         */
        setProviders(state, providers) {
            state.all = providers
        },
        /**
         * Add provider to forms array
         * @param {Object} state
         * @param {Object} provider
         */
        addToProviderForm(state, provider) {
            const data = provider
                ? {...provider}
                : {
                    _id: null,
                    name: ''
                }
            data.actions = {
                edit: {
                    active: !data._id,
                    loading: false
                },
                remove: {
                    loading: false
                }
            }
            data.error = {}
            state.forms.push(data)
        },
        /**
         * Set name to provider in form
         * @param {Object} state
         * @param {Number} form_index
         * @param {String} name
         */
        setFormName(state, {form_index, name}) {
            state.forms[form_index].name = name
        },
        /**
         * Disable edit mode of provider
         * @param {Object} state
         * @param {Number} form_index
         */
        cancelForm(state, form_index) {
            if (state.forms[form_index]._id !== null) {
                state.forms[form_index].actions.edit.active = false
            } else {
                state.forms.splice(form_index, 1)
            }
        },
        /**
         * Update providers with the saved one
         * @param {Object} state
         * @param {Object} provider
         */
        updateProvidersWith(state, provider) {
            let i = findIndex(state.all, {_id: provider._id})
            if (i !== -1) {
                Vue.set(state.all, i, provider)
            } else {
                state.all.push(provider)
            }
        },
        /**
         * Update provider in form array and disable edit mode
         * @param {Object} state
         * @param {Object} provider
         * @param {Number} form_index
         */
        saveFormAndClose(state, {provider, form_index}) {
            state.forms[form_index]._id = provider._id
            state.forms[form_index].actions.edit.active = false
        },
        /**
         * Enable/disable provider form preloader
         * @param {Object} state
         * @param {Number} form_index
         * @param {Boolean} loading
         */
        setFormProviderLoading(state, {form_index, loading}) {
            state.forms[form_index].actions.edit.loading = loading
        },
        /**
         * Set errors of provider form
         * @param {Object} state
         * @param {Number} form_index
         * @param {Object} error
         */
        setFormProviderError(state, {form_index, error}) {
            state.forms[form_index].error = error
        },
        /**
         * Remove provider from form
         * @param {Object} state
         * @param {Number} form_index
         */
        deleteProviderForm(state, form_index) {
            state.forms.splice(form_index, 1)
        },
        /**
         * Remove provider from providers list
         * @param {Object} state
         * @param {Object} provider
         */
        deleteProvider(state, provider) {
            let i = findIndex(state.all, {_id: provider._id})
            if (i !== -1) {
                state.all.splice(i, 1)
            }
        }
    }
}

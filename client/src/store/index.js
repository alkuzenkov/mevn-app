import Vue from 'vue'
import Vuex from 'vuex'
import clients from '@/store/modules/clients'
import providers from '@/store/modules/providers'

Vue.use(Vuex)

export default new Vuex.Store({
    modules: {
        clients,
        providers
    }
})

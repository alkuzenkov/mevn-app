import api from '../lib/api'

export default {
    /**
     * Retrieve all clients
     * @returns {Promise<Array>}
     */
    async all() {
        let response
        try {
            response = await api().get('clients')
        } catch (e) {
            return []
        }
        return response.data
    },
    /**
     * Save client
     * @param {Object} client
     * @returns {Promise<Object>}
     */
    async save(client) {
        return new Promise(async (resolve, reject) => {
            try {
                if (client._id === null) {
                    resolve(await this.store(client))
                } else {
                    resolve(await this.update(client))
                }
            } catch (error) {
                reject(error)
            }
        })
    },
    /**
     * Create client
     * @param {Object} client
     * @returns {Promise<Object>}
     */
    async store(client) {
        return new Promise(async (resolve, reject) => {
            let response
            try {
                response = await api().post('clients/store', {
                    id: client._id,
                    name: client.name,
                    email: client.email,
                    phone: client.phone,
                    providers: client.providers,
                })
            } catch (error) {
                reject(error.response.data)
                return
            }
            resolve(response.data)
        })
    },
    /**
     * Update client
     * @param {Object} client
     * @returns {Promise<Object>}
     */
    async update(client) {
        return new Promise(async (resolve, reject) => {
            let response
            try {
                response = await api().patch('clients/' + client._id, {
                    name: client.name,
                    email: client.email,
                    phone: client.phone,
                    providers: client.providers,
                })
            } catch (error) {
                reject(error.response.data)
                return
            }
            resolve(response.data)
        })
    },
    /**
     * Remove client
     * @param {Object} client
     * @returns {Promise<Object>}
     */
    async remove(client) {
        return new Promise(async (resolve, reject) => {
            let response
            try {
                response = await api().delete('clients/' + client._id)
            } catch (error) {
                reject(error.response.data)
                return
            }
            resolve(response.data)
        })
    }
}

import api from '../lib/api'

export default {
    /**
     * Retrieve all providers
     * @returns {Promise<Array>}
     */
    async all() {
        let response
        try {
            response = await api().get('providers')
        } catch (e) {
            return []
        }
        return response.data
    },
    /**
     * Save provider
     * @param {Object} provider
     * @returns {Promise<Object>}
     */
    async save(provider) {
        return new Promise(async (resolve, reject) => {
            try {
                if (provider._id === null) {
                    resolve(await this.store(provider))
                } else {
                    resolve(await this.update(provider))
                }
            } catch (error) {
                reject(error)
            }
        })
    },
    /**
     * Create provider
     * @param {Object} provider
     * @returns {Promise<Object>}
     */
    async store(provider) {
        return new Promise(async (resolve, reject) => {
            let response
            try {
                response = await api().post('providers/store', {
                    id: provider._id,
                    name: provider.name
                })
            } catch (error) {
                reject(error.response.data)
                return
            }
            resolve(response.data)
        })
    },
    /**
     * Update provider
     * @param {Object} provider
     * @returns {Promise<Object>}
     */
    async update(provider) {
        return new Promise(async (resolve, reject) => {
            let response
            try {
                response = await api().patch('providers/' + provider._id, {
                    name: provider.name
                })
            } catch (error) {
                reject(error.response.data)
                return
            }
            resolve(response.data)
        })
    },
    /**
     * Remove provider
     * @param {Object} provider
     * @returns {Promise<Object>}
     */
    async remove(provider) {
        return new Promise(async (resolve, reject) => {
            let response
            try {
                response = await api().delete('providers/' + provider._id)
            } catch (error) {
                reject(error.response.data)
                return
            }
            resolve(response.data)
        })
    }
}

import 'swagger-ui/dist/swagger-ui.css'
import SwaggerUI from 'swagger-ui'

SwaggerUI({
    dom_id: '#app',
    url: 'openapi.yaml'
})
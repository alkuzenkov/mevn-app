### Docker instructions
Set up
- `docker-compose build`
- `docker-compose up`

Hosts
- 'http://0.0.0.0:8080' - Client
- 'http://0.0.0.0:8081' - Server
- 'http://0.0.0.0:27017' - MongoDB
- 'http://0.0.0.0:8082' - Swagger

### Non-Docker instructions

Client set up
- `cd client`
- `yarn`
- `yarn start`

Server and Database set up
- `cd server`
- `yarn`
- `cp nodemon.example.json nodemon.json`
- Set up your database local configuration in `nodemon.json`
- `yarn start`

Swagger set up 
- `cd swagger`
- `yarn`
- `yarn start`
 
Hosts
- 'http://0.0.0.0:8080' - Client
- 'http://0.0.0.0:8081' - Server
- 'http://{YOUR_MONGODB_HOST}:27017' - MongoDB
- 'http://0.0.0.0:8082' - Swagger
const Mongo = require('@/lib/mongo/Mongo')
const config = require('@/config/db')

/**
 * Main MongoDB client
 */
class Main {

    /**
     * Main client constructor
     */
    constructor() {
        this.instance = new Mongo(config)
        this.instance.connect()
    }

    /**
     * Getting current client connection
     * @returns {Mongoose}
     */
    get mongoose() {
        return this.instance.mongoose
    }

}

module.exports = Main
const mongoose = require('mongoose')

/**
 * Database class
 */
class Mongo {

    /**
     * Instance creation
     * @param {Object} config
     */
    constructor(config) {
        this.host = config.HOST
        this.database = config.DATABASE
        this.port = config.PORT
        this.username = config.USERNAME
        this.password = config.PASSWORD
        this.options = config.OPTIONS
    }

    /**
     * Connection creation
     */
    async connect() {
        const readyState = this.mongoose.connection.readyState
        if (readyState === 1 || readyState === 2) {
            return
        }
        try {
            await this.mongoose.connect(this.connectString, {
                useNewUrlParser: true
            })
        } catch (error) {
            if (error.message && error.message.match(/failed to connect to server .* on first connect/)) {
                setTimeout(() => this.connect(), 5e3)
            }
            console.error(new Date, String(error))
        }
    }

    /**
     * Get final connection string
     * @returns {String}
     */
    get connectString() {
        const userPass = this.username ? `${this.username}:${this.password}@` : ''
        const options = this.options ? '?' + this.options : ''
        return `mongodb://${userPass}${this.host}:${this.port}/${this.database}${options}`
    }

    /**
     * Get MongoDB client instance
     * @returns {Mongoose}
     */
    get mongoose() {
        return mongoose
    }

}

module.exports = Mongo
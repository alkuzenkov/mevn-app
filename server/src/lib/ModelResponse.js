module.exports = class ModelResponse {

    /**
     * Class constructor
     */
    constructor() {
        this.success = false
        this.message = ''
        this.error = {}
        this.model = {}
    }

    /**
     * Parse errors from model
     * @param {Object} error
     */
    parseModelErrors(error) {
        for (const [name, info] of Object.entries(error.errors)) {
            this.error[name] = info.message
        }
    }

    /**
     * Set operation status
     * @param {Boolean} success
     */
    setSuccess(success) {
        this.success = success
    }

    /**
     * Set mongoose model
     * @param {Model} model
     */
    setModel(model) {
        this.model = {...model._doc}
        delete this.model.__v
    }

    /**
     * Set response message
     * @param {String} message
     */
    setMessage(message) {
        this.message = message
    }

    /**
     * Get response HTTP status code
     * @returns {Number}
     */
    getStatusCode() {
        return this.success ? 200 : 400
    }

    /**
     * Get response body
     * @returns {Object}
     */
    getBody() {
        let body = {
            message: this.message
        }
        if (this.success) {
            body.model = this.model
        } else {
            body.error = this.error
        }
        return body
    }

    /**
     * Send HTTP response
     * @param {ServerResponse} res
     */
    sendResponse(res) {
        res.status(this.getStatusCode())
        res.send(this.getBody())
    }

    /**
     * Send error response
     * @param {ServerResponse} res
     * @param {String} message
     * @param {Object} model_error
     */
    static errorResponse(res, message, model_error) {
        let _this = new ModelResponse
        if (model_error) {
            _this.parseModelErrors(model_error)
        }
        _this.setSuccess(false)
        _this.setMessage(message)
        _this.sendResponse(res)
    }

    /**
     * Send successful response
     * @param {ServerResponse} res
     * @param {String} message
     * @param {Model} model
     */
    static successResponse(res, message, model) {
        let _this = new ModelResponse
        if (model) {
            _this.setModel(model)
        }
        _this.setSuccess(true)
        _this.setMessage(message)
        _this.sendResponse(res)
    }

}

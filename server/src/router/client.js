const express = require('express')
const router = express.Router()
const controller = require('@/controllers/client')

router.get('/', controller.all)
router.post('/store', controller.store)
router.patch('/:clientId', controller.update)
router.delete('/:clientId', controller.destroy)

module.exports = router

const express = require('express')
const router = express.Router()
const controller = require('@/controllers/provider')

router.get('/', controller.all)
router.post('/store', controller.store)
router.patch('/:providerId', controller.update)
router.delete('/:providerId', controller.destroy)

module.exports = router

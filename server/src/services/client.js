const clientModel = require('@/models/client')

module.exports = {
    /**
     * Get all clients from DB
     * @returns {Promise<Array>}
     */
    async all() {
        return await clientModel.find({}).exec()
    },
    /**
     * Validate client and save
     * @param {Model} client
     * @returns {Promise<Object>}
     */
    validateAndSave(client) {
        return new Promise(async (resolve, reject) => {
            let error = client.validateSync()
            if (error) {
                reject(error)
            }
            try {
                client = await client.save()
            } catch (error) {
                reject(error)
            }
            resolve(client)
        })
    }
}

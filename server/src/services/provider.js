module.exports = {
    /**
     * Validate provider and save
     * @param {Model} provider
     * @returns {Promise<Object>}
     */
    validateAndSave(provider) {
        return new Promise(async (resolve, reject) => {
            let error = provider.validateSync()
            if (error) {
                reject(error)
            }
            try {
                provider = await provider.save()
            } catch (error) {
                reject(error)
            }
            resolve(provider)
        })
    }
}

const providerModel = require('@/models/provider')
const clientModel = require('@/models/client')
const ModelResponse = require('@/lib/ModelResponse')
const providerService = require('@/services/provider')

/**
 * Save provider to DB and send HTTP response
 * @param {ServerResponse} res
 * @param {Model} provider
 * @returns {Promise<void>}
 */
async function saveProviderAndSendResponse(res, provider) {
    try {
        provider = await providerService.validateAndSave(provider)
    } catch (error) {
        ModelResponse.errorResponse(res, 'Validation error', error)
        return
    }
    ModelResponse.successResponse(res, 'Provider saved', provider)
}

module.exports = {
    /**
     * Get all providers
     * @param {IncomingMessage} req
     * @param {ServerResponse} res
     * @param {Function} next
     * @returns {Promise<void>}
     */
    async all(req, res, next) {
        try {
            res.send(await providerModel.find({}).exec())
        } catch (e) {
            next(e)
        }
    },
    /**
     * Create provider
     * @param {IncomingMessage} req
     * @param {ServerResponse} res
     * @returns {Promise<void>}
     */
    async store(req, res) {
        saveProviderAndSendResponse(res, new providerModel({
            name: req.body.name
        }))
    },
    /**
     * Update provider
     * @param {IncomingMessage} req
     * @param {ServerResponse} res
     * @returns {Promise<void>}
     */
    async update(req, res) {
        let provider
        try {
            provider = await providerModel.findById(req.params.providerId)
        } catch (error) {
            ModelResponse.errorResponse(res, 'Provider not found')
            return
        }
        provider.name = req.body.name
        saveProviderAndSendResponse(res, provider)
    },
    /**
     * Delete provider
     * @param {IncomingMessage} req
     * @param {ServerResponse} res
     * @returns {Promise<void>}
     */
    async destroy(req, res) {
        let provider
        try {
            provider = await providerModel.findById(req.params.providerId)
        } catch (error) {
            ModelResponse.errorResponse(res, 'Provider not found')
            return
        }
        try {
            clientModel.update({}, {
                    $pull: {
                        providers: {
                            $elemMatch: {
                                _id: provider._id
                            }
                        }
                    },
                },
                {
                    multi: true
                })
            provider = await provider.remove()
        } catch (error) {
            ModelResponse.errorResponse(res, 'Provider not removed')
            return
        }
        ModelResponse.successResponse(res, 'Provider removed', provider)
    }
}

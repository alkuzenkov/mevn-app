const clientModel = require('@/models/client')
const ModelResponse = require('@/lib/ModelResponse')
const clientService = require('@/services/client')

/**
 * Save client to DB and send HTTP response
 * @param {ServerResponse} res
 * @param {Model} client
 * @returns {Promise<void>}
 */
async function saveClientAndSendResponse(res, client) {
    try {
        client = await clientService.validateAndSave(client)
    } catch (error) {
        ModelResponse.errorResponse(res, 'Validation error', error)
        return
    }
    ModelResponse.successResponse(res, 'Client saved', client)
}

module.exports = {
    /**
     * Get all clients
     * @param {IncomingMessage} req
     * @param {ServerResponse} res
     * @param {Function} next
     * @returns {Promise<void>}
     */
    async all(req, res, next) {
        try {
            res.send(await clientService.all())
        } catch (e) {
            next(e)
        }
    },
    /**
     * Create a new client
     * @param {IncomingMessage} req
     * @param {ServerResponse} res
     * @returns {Promise<void>}
     */
    async store(req, res) {
        saveClientAndSendResponse(res, new clientModel({
            name: req.body.name,
            email: req.body.email,
            phone: req.body.phone,
            providers: req.body.providers
        }))
    },
    /**
     * Update client
     * @param {IncomingMessage} req
     * @param {ServerResponse} res
     * @returns {Promise<void>}
     */
    async update(req, res) {
        let client
        try {
            client = await clientModel.findById(req.params.clientId)
        } catch (error) {
            ModelResponse.errorResponse(res, 'Client not found')
            return
        }
        client.name = req.body.name
        client.email = req.body.email
        client.phone = req.body.phone
        client.providers = req.body.providers
        saveClientAndSendResponse(res, client)
    },
    /**
     * Delete a client
     * @param {IncomingMessage} req
     * @param {ServerResponse} res
     * @returns {Promise<void>}
     */
    async destroy(req, res) {
        let client
        try {
            client = await clientModel.findById(req.params.clientId)
        } catch (error) {
            ModelResponse.errorResponse(res, 'Client not found')
            return
        }
        try {
            client = await client.remove()
        } catch (error) {
            ModelResponse.errorResponse(res, 'Client not removed')
            return
        }
        ModelResponse.successResponse(res, 'Client removed', client)
    }
}

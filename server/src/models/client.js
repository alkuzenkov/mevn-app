const DB = require('@connections/Main')
const uniqueValidator = require('mongoose-unique-validator')
const mongoose = new DB().mongoose

const providerSchema = new mongoose.Schema({
    _id: String
})

const clientSchema = new mongoose.Schema({
    name: {
        type: String,
        trim: true,
        minlength: [2, 'Minimum length is 2'],
        required: [true, 'Name is required']
    },
    email: {
        type: String,
        trim: true,
        unique: true,
        required: [true, 'Email is required'],
        validate: {
            validator(value) {
                return /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(value)
            },
            message(props) {
                return `${props.value} is not a valid email address!`
            }
        },
    },
    phone: {
        type: String,
        trim: true,
        unique: true,
        required: [true, 'Phone is required'],
        validate: {
            validator(v) {
                return /\d{3}-\d{3}-\d{4}/.test(v)
            },
            message(props) {
                return `${props.value} is not a valid phone number! Please, provide the number in following format: XXX-XXX-XXXX`
            }
        },
    },
    providers: {
        type: [providerSchema],
        validate: {
            async validator(providers) {
                const providerModel = require('@/models/provider')
                for (let provider of providers) {
                    try {
                        await providerModel.findById(provider._id)
                    } catch (error) {
                        return false;
                    }
                }
                return true
            },
            message() {
                return `One of the providers in not exists`
            }
        }
    }
})

providerSchema.set('toJSON', {
    versionKey: false
})

providerSchema.plugin(uniqueValidator)

module.exports = mongoose.model('Client', clientSchema)

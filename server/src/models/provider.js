const DB = require('@connections/Main')
const uniqueValidator = require('mongoose-unique-validator')

const mongoose = new DB().mongoose

const providerSchema = new mongoose.Schema({
    name: {
        type: String,
        unique: true,
        trim: true,
        minlength: [2, 'Minimum length is 2'],
        required: [true, 'Name is required']
    }
})

providerSchema.plugin(uniqueValidator)

providerSchema.set('toJSON', {
    versionKey: false
})

module.exports = mongoose.model('Provider', providerSchema)

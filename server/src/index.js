require('module-alias/register')
const morgan = require('morgan')
const express = require('express')
const cors = require('cors')
const bodyParser = require('body-parser');
const app = express()

app.use(morgan('combined'))
app.use(cors())
app.use(bodyParser.json());
app.use('/clients', require('@/router/client'))
app.use('/providers', require('@/router/provider'))
app.use(function (err, req, res, next) {
    if (res.headersSent) {
        return next(err)
    }
    console.error(err.stack)
    res.status(500).send()
})

app.listen(process.env.LISTEN_PORT)